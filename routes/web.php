<?php

// Route::get('/', function () {
//     return view('welcome');
// });Route::get('/bt', function () {
//     return view('bt');
// });
//get bisa di akses pada url, jika mengandung key key akan di tampilkan pada url
route::get('/','nc@main');
route::get('/panggil','nc@panggil');
route::get('/panggilulang','nc@panggilulang');
route::get('/test','nc@test');
route::get('/tambah','nc@tambah');
route::get('/edit/{id}','nc@edit');
//post tidak bisa di aksess melalui url
//post biasanya untuk create
route::post('/opertambah','nc@opertambah');
//put biasanya untuk update tp d jajal gung kenek :v
route::post('/operedit/{id}','nc@operedit');
route::delete('/hapus/{id}','nc@hapus');
route::delete('/ambil','nc@ambil');
route::delete('/reset','nc@reset');