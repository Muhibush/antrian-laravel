@extends('template.template1')
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
@section('isi')
	<div class="container" style="font-family: sans-serif;">
		<div id="hasil" class="title m-b-md">Nomor antrian </div>
		{{-- <div id="test" class="title m-b-md">test : </div> --}}
		<table>
			<tr>
				<td class="col-3">
					<form>
				        <input type="button" value="panggil ulang" class="btn btn-success" id="panggilulang">
				    </form>
				</td>
				<td class="col-3">
					<form action="/tambah" method="get">
				        <input type="submit" value="tambah" id="tambah" 	class="btn btn-success">
				        {{csrf_field()}}   
				    </form>
				</td>
				<td class="col-3">
					<form action="/ambil" method="post">
				        <input type="submit" value="ambil" class="btn btn-primary" id="ambil">
				        {{csrf_field()}}   
				        <input type="hidden" name="_method" value="delete">
				    </form>
				</td>
				<td class="col-3">
					<form action="/reset" method="post">
				        <input type="submit" value="reset" class="btn btn-primary">
				        {{csrf_field()}}   
				        <input type="hidden" name="_method" value="delete">
				    </form>
				</td>
			</tr>
		</table>
		<table class="table table-hover table-bordered">
				<tr>
					<th class="col-md-2">Nomor urut</th>
					<th class="col-md-4">Nama</th>
					<th class="col-md-4">Alamat</th>
					<th class="col-md-2">Aksi</th>
				</tr>
				@foreach ($isi as $isi)
				<tr>
					<td class="col-md-2">{{$isi->id}}</td>
					<td class="col-md-4">{{$isi->nama}}</td>
					<td class="col-md-4">{{$isi->alamat}}</td>
{{-- 					<td class="col-md-1">
						<form action="/hapus/{{$isi->id}}" method="post">
					        <input type="submit" value="hapus" class="btn btn-danger">
					        {{csrf_field()}}   
					        <input type="hidden" name="_method" value="delete">
					    </form>
					</td> --}}
					<td class="col-md-2">
						{{-- <input type="button" value="edit" onclick="window.location = '/edit/{{$isi->id}}';"/> --}}
						<form action="/edit/{{$isi->id}}" method="post">
					        <input type="submit" value="edit" class="btn btn-warning">
					        {{csrf_field()}}   
					        <input type="hidden" name="_method" value="get">
					    </form>
					</td>
				</tr>
				@endforeach
		</table>
	</div>

	<script>
		var diAtasTombol = document.getElementById("ambil"),
			hasil = document.getElementById("hasil"),
			test = document.getElementById("test"),
			tambah = document.getElementById("tambah").value,
				tidakadalayanan = {{$tidakadalayanan}},
				dalamlayanan = {{$dalamlayanan}},
				kosong = {{$kosong}};

		function load(){
				if (kosong==1) {
					document.getElementById("ambil").disabled = true;
				}
				if (tidakadalayanan==1) {
					document.getElementById("panggilulang").disabled = true;
				}
			if(dalamlayanan >= "1"){
				hasil.innerHTML += "<p>"+ {{$dalamlayanan}} +"</p>";
			}
		}
	</script>
@endsection