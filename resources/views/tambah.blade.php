@extends('template.template1')
@section('isi')
    <form action="/opertambah" method="post">
        <input type="text" name="nama" placeholder="nama anda" value="{{old('nama')}}">
        @if ($errors->has('nama'))
            <p>{{$errors->first('nama')}}</p>
        @endif
        <input type="text" name="alamat" placeholder="alamat anda" value="{{old('alamat')}}">
        @if ($errors->has('alamat'))
            <p>{{$errors->first('alamat')}}</p>
        @endif
        <input type="submit">
        {{csrf_field()}}   
    </form>
    
@endsection