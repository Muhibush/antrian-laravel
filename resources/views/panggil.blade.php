@extends('template.template1')
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
@section('isi')
	<div class="container" style="font-family: sans-serif;">
		<div id="hasil" class="title m-b-md">Nomor antrian </div>
		{{-- <div id="test" class="title m-b-md">test : </div> --}}
		<table>
			<tr>
				<td class="col-3">
					<form>
				        <input type="button" value="panggil ulang" class="btn btn-success" id="panggilulang">
				    </form>
				</td>
				<td class="col-3">
					<form action="/tambah" method="get">
				        <input type="submit" value="tambah" id="tambah" 	class="btn btn-success">
				        {{csrf_field()}}   
				    </form>
				</td>
				<td class="col-3">
					<form action="/ambil" method="post">
				        <input type="submit" value="ambil" class="btn btn-primary" id="ambil">
				        {{csrf_field()}}   
				        <input type="hidden" name="_method" value="delete">
				    </form>
				</td>
				<td class="col-3">
					<form action="/reset" method="post">
				        <input type="submit" value="reset" class="btn btn-primary">
				        {{csrf_field()}}   
				        <input type="hidden" name="_method" value="delete">
				    </form>
				</td>
			</tr>
		</table>
		<table class="table table-hover table-bordered">
				<tr>
					<th class="col-md-2">Nomor urut</th>
					<th class="col-md-4">Nama</th>
					<th class="col-md-4">Alamat</th>
					<th class="col-md-2">Aksi</th>
				</tr>
				@foreach ($isi as $isi)
				<tr>
					<td class="col-md-2">{{$isi->id}}</td>
					<td class="col-md-4">{{$isi->nama}}</td>
					<td class="col-md-4">{{$isi->alamat}}</td>
{{-- 					<td class="col-md-1">
						<form action="/hapus/{{$isi->id}}" method="post">
					        <input type="submit" value="hapus" class="btn btn-danger">
					        {{csrf_field()}}   
					        <input type="hidden" name="_method" value="delete">
					    </form>
					</td> --}}
					<td class="col-md-2">
						{{-- <input type="button" value="edit" onclick="window.location = '/edit/{{$isi->id}}';"/> --}}
						<form action="/edit/{{$isi->id}}" method="post">
					        <input type="submit" value="edit" class="btn btn-warning">
					        {{csrf_field()}}   
					        <input type="hidden" name="_method" value="get">
					    </form>
					</td>
				</tr>
				@endforeach
		</table>
	</div>
	<script>
		var nomorantrian = new Audio('/audio/nomorantrian.mp3'),
			kosong = new Audio('/audio/kosong.mp3'),
			satu = new Audio('/audio/satu.mp3'),
			dua = new Audio('/audio/dua.mp3'),
			tiga = new Audio('/audio/tiga.mp3'),
			empat = new Audio('/audio/empat.mp3'),
			lima = new Audio('/audio/lima.mp3'),
			enam = new Audio('/audio/enam.mp3'),
			tujuh = new Audio('/audio/tujuh.mp3'),
			delapan = new Audio('/audio/delapan.mp3'),
			sembilan = new Audio('/audio/sembilan.mp3'),
			sepuluh = new Audio('/audio/sepuluh.mp3'),
			sebelas = new Audio('/audio/sebelas.mp3'),
			belas = new Audio('/audio/belas.mp3'),
			puluh = new Audio('/audio/puluh.mp3'),
			diAtasTombol = document.getElementById("ambil"),
			hasil = document.getElementById("hasil"),
			test = document.getElementById("test"),
			tambah = document.getElementById("tambah").value,
			tidakadalayanan = {{$tidakadalayanan}},
			dalamlayanan = {{$dalamlayanan}},
			kosong = {{$kosong}};

		document.getElementById("panggilulang").addEventListener("click", function(){
			panggil();
		});
		function load(){
			if (kosong==1) {
				document.getElementById("ambil").disabled = true;
			}
			if (tidakadalayanan==1) {
				
				document.getElementById("panggilulang").disabled = true;
			}
			hasil.innerHTML += "<p>"+ {{$dalamlayanan}} +"</p>";
			panggil();
		}
		function panggil(){
			if(dalamlayanan >= "1"){
				nomorantrian.play();

				if (dalamlayanan <= "9") {
					setTimeout(function() {suara(dalamlayanan);}, 2000);
					
				}
				if (dalamlayanan >= "10" && dalamlayanan <= "11") {
					if (dalamlayanan=="10") {
						setTimeout(function() {sepuluh.play();}, 2000);
					}
					if (dalamlayanan=="11") {
						setTimeout(function() {sebelas.play();}, 2000);
					}
				}
				if (dalamlayanan >= "12" && dalamlayanan <= "19") {
					var angkakedua = dalamlayanan.toString().slice(1,2);
					setTimeout(function() {
						suara(angkakedua);
						setTimeout(function() {
							belas.play();
						}, 900);
					}, 2000);
				}
				if (dalamlayanan >= "20" && dalamlayanan <= "99") {
					var angkapertama = dalamlayanan.toString().slice(0,1),
						angkakedua = dalamlayanan.toString().slice(1,2);
					setTimeout(function() {
						suara(angkapertama);
						setTimeout(function() {
							puluh.play();
							setTimeout(function() {
								suara(angkakedua);
							}, 900);
						}, 900);
					}, 2000);
				}
			}
		}
		function suara(kodesuara){
			if (kodesuara=="1") {
				satu.play();
			}
			if (kodesuara=="2") {
				dua.play();
			}
			if (kodesuara=="3") {
				tiga.play();
			}
			if (kodesuara=="4") {
				empat.play();
			}
			if (kodesuara=="5") {
				lima.play();
			}
			if (kodesuara=="6") {
				enam.play();
			}
			if (kodesuara=="7") {
				tujuh.play();
			}
			if (kodesuara=="8") {
				delapan.play();
			}
			if (kodesuara=="9") {
				sembilan.play();
			}
		}
	</script>
@endsection