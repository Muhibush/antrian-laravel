@extends('template.template1')
@section('isi')
    <form action="/operedit/{{$isi->id}}" method="post">
        <input type="text" name="nama" value="{{$isi->nama}}">
        @if ($errors->has('nama'))
        	<p>{{$errors->first('nama')}}</p>
        @endif
        <input type="text" name="alamat" value="{{$isi->alamat}}">
        @if ($errors->has('alamat'))
        	<p>{{$errors->first('alamat')}}</p>
        @endif
        <input type="submit" value="simpan">
        {{csrf_field()}}   
    </form>
@endsection