<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\pengguna;
class nc extends Controller
{   
    public function main(){
        $dalamlayanan=0;
        $tidakadalayanan=0;
        $data=pengguna::all();
        $kosong=0;
        $dataasli=pengguna::withTrashed()->get();
        if(count($data)!=0){
            $tampilpertama=$data->first()->id;
            $dalamlayanan=$tampilpertama - 1;
        }
        if(count($data)==0&&count($dataasli)!=0){
            $dalamlayanan=$dataasli->last()->id;
        }
        if(count($data)==0){
            $kosong=1;
        }
        if($dalamlayanan==0){
            $tidakadalayanan=1;
        }
        return view('main',['isi'=>$data,'dalamlayanan'=>$dalamlayanan,'kosong'=>$kosong,'tidakadalayanan'=>$tidakadalayanan]);
    }
    public function panggil(){
        $dalamlayanan=0;
        $tidakadalayanan=0;
        $data=pengguna::all();
        $kosong=0;
        $dataasli=pengguna::withTrashed()->get();
        if(count($data)!=0){
            $tampilpertama=$data->first()->id;
            $dalamlayanan=$tampilpertama - 1;
        }
        if(count($data)==0&&count($dataasli)!=0){
            $dalamlayanan=$dataasli->last()->id;
        }
        if(count($data)==0){
            $kosong=1;
        }
        if($dalamlayanan==0){
            $tidakadalayanan=1;
        }
        return view('panggil',['isi'=>$data,'dalamlayanan'=>$dalamlayanan,'kosong'=>$kosong,'tidakadalayanan'=>$tidakadalayanan]);
    }
    public function tambah(){
    	return view('tambah');
    }
    public function opertambah(Request $request){
        // $this->validate($request,[
        //     'nama' => 'required|min:5',
        //     'alamat' => 'required',
        //     ]);
    	$pengguna = new pengguna;
    	$pengguna->nama=$request->nama;
    	$pengguna->alamat=$request->alamat;
    	$pengguna->save();
    	return redirect('/');
    }
    public function edit($id){
    	$data=pengguna::find($id);
    	return view('edit',['isi'=>$data]);
    }
    public function operedit(Request $request,$id){
        // $this->validate($request,[
        //     'nama' => 'required|min:5',
        //     'alamat' => 'required',
        //     ]);
    	$pengguna = pengguna::find($id);
    	$pengguna->nama=$request->nama;
    	$pengguna->alamat=$request->alamat;
    	$pengguna->save();
    	return redirect('panggil');
    }
    public function hapus($id){
    	$data=pengguna::find($id);
    	$data->delete();
    	return redirect('/');
    }
    public function ambil(){
        $data=pengguna::all();
        $data->first()->delete();
        return redirect('panggil');
    }
    public function reset(){
        $data=pengguna::truncate();
        return redirect('/');
    }
    public function panggilulang(){
        return redirect('panggil');
    }
}
